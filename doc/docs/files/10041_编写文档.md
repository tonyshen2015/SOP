# 编写文档

作为开放平台，必须要提供API文档。

SOP采用微服务架构实现，因此文档应该由各个微服务各自实现。难点就是如何统一归纳各个微服务端提供的文档信息，并且统一展示。

写完接口后使用swagger注解来定义自己的文档信息。步骤如下：

- maven添加swagger

```xml
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger2</artifactId>
    <version>2.9.2</version>
    <exclusions>
        <exclusion>
            <groupId>io.swagger</groupId>
            <artifactId>swagger-models</artifactId>
        </exclusion>
    </exclusions>
</dependency>
<dependency>
    <groupId>io.swagger</groupId>
    <artifactId>swagger-models</artifactId>
    <version>1.5.21</version>
</dependency>
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger-ui</artifactId>
    <version>2.9.2</version>
</dependency>

```

- 在config中添加swagger配置

```java
@Configuration
public class OpenServiceConfig extends AlipayServiceConfiguration {
    // 开启文档
    @Configuration
    @EnableSwagger2
    public static class Swagger2 extends SwaggerSupport {
        @Override
        protected String getDocTitle() {
            // 不能重复。比如订单服务返回：`订单API`；库存服务返回：`库存API`
            return "图书API";
        }
    }
}
```

其中`getDocTitle()`返回文档模块名，不能和其它微服务重复。比如订单服务返回：`订单API`；库存服务返回：`库存API`

- 编写swagger注解

分别在请求参数和返回结果类中编写`@ApiModelProperty`

```java
// 请求参数
@Data
public class BookParam {
    @ApiModelProperty(value = "图书id", example = "1")
    private int id;

    @ApiModelProperty(value = "图书ISBN", example = "xxxx")
    private String isbn;
}

// 返回结果
@Data
public class BookVO {
    @ApiModelProperty(value = "图书id", example = "1")
    private int id;

    @ApiModelProperty(value = "图书名称", example = "白雪公主")
    private String name;

    @ApiModelProperty(value = "isbn", example = "xxxxxx")
    private String isbn;
}
```

- 在接口方法上编写`@ApiOperation`注解

```java
@ApiOperation(value="查询书本信息", notes = "可以根据ISBN查询书本信息")
@ApiMapping(value = "book.search")
public BookVO searchBook(BookParam param) {
    BookVO bookVO = new BookVO();
    bookVO.setId(1);
    bookVO.setName("白雪公主，ISBN:" + param.getIsbn());
    bookVO.setIsbn("ABCSSSSDDD");
    return bookVO;
}
```

其中`value`属性填接口名称，简明扼要。`notes`填写接口的详细信息，介绍，用途，注意事项等。

## 查看文档

- 启动website-server(运行WebsiteServerApplication.java)
- 找到sop-website/website-front/pages/doc/doc.html，IDEA下右键--Debug

如果没有IDEA可以下个webstorm，同样有本地静态服务器功能。

效果图如下

![预览](images/10041_1.png "10041_1.png")

## 注解对应关系

swagger注解和文档界面显示关系如下图所示：

![预览](images/10041_2.png "10041_2.png")


![预览](images/10041_3.png "10041_3.png")