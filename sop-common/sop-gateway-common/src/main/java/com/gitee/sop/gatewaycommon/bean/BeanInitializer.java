package com.gitee.sop.gatewaycommon.bean;

/**
 * @author tanghc
 */
public interface BeanInitializer {
    /**
     * 执行加载操作
     */
    void load();
}
